#!/bin/bash
echo "MOUNTING"
mount -t proc /proc /home/dazen/linux/rootfs/rootfs/proc
mount -t sysfs /sys /home/dazen/linux/rootfs/rootfs/sys
mount -o bind /dev /home/dazen/linux/rootfs/rootfs/dev
mount -o bind /dev/pts /home/dazen/linux/rootfs/rootfs/dev/pts
chroot /home/dazen/linux/rootfs/rootfs/
